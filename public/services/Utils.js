const Utils = { 
    
    parseRequestURL : () => {

        let url = location.hash.slice(1) || '/';
        let r = url.split("/")
        let request = {
            resource    : null,
            id          : null,
            verb        : null
        }
        request.resource    = r[1]
        request.id          = r[2]
        request.verb        = r[3]

        return request
    }

    , sleep: (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    },
    strip : (html) => {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    },

    excerpt : function(text, length)      {
        text = this.strip(text);
        text = $.trim(text); //trim whitespace
        if (text.length > length) {
            text = text.substring(0, length - 3) + '...';
        }
        return text;
    },

    pad2Digit: (num) => {
        return ('0' + num.toString()).slice(-2);
    },

    datetimeFormat : function(timestamp) {
        var dateObj = new Date(timestamp);
        var en_month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return dateObj.getDate() + ' ' + en_month[dateObj.getMonth()] + ' ' + this.pad2Digit(dateObj.getFullYear()) + ' ' + this.pad2Digit(dateObj.getHours()) + ':' + this.pad2Digit(dateObj.getMinutes());
    },
    $_GET : function(key) {
        var queries = window.location.href.split('?').pop().split('&');
        var params = {};
        queries.map(function (query) {
            var set = query.split('=');
            params[set[0]] = set[1];
        });

        if (key) {
            return params[key] || null;
        } else {
            return params;
        }

    }
}

export default Utils;