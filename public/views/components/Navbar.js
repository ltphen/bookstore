let Navbar = {
    render: async () => {
        let view =  /*html*/
            `
               <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Firebase</a>
                </div>
                <div class="collapse navbar-collapse" id="nav">
                    <ul class="nav navbar-nav">
                        <li><a href="/#/login">Member</a></li>
                        <li><a href="/#/create">New Entry</a></li>
                    </ul>
                </div>
            </div>
        </nav>
            
            `
        return view
    },
    after_render: async () => { }

}


export default Navbar;