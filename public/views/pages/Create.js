

let Create = {
    render : async () => {

        let view =  /*html*/`
        <form id="my_form">
        <h2>Title</h2>
        <br>
        <input type="text" name="title" class="form-control col-md-12" required>
        
        <br>
        <br>
        
        <h2>Content</h2>
        <br>
        <textarea name="content" id="content"></textarea>
        
        <br>
        <br>
        
        <div class="text-right">
            <button class="btn btn-large btn-primary">Create new entry</button>
        </div>
    </form>
            </section>
        `
        return view
    }
    , after_render: async () => {
          
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) { 
                // init CKEditor
                CKEDITOR.replace('content');
                
                /***************************************************\
                 * Process form data and save to Firebase database *
                \***************************************************/
               
                $('#my_form').submit(function(e){
                    e.preventDefault();
                
                    var entry = {};
                    entry.title = $(this).find('[name="title"]').val();
                    entry.content = CKEDITOR.instances['content'].getData();
                    entry.createdAt = new Date().getTime();
                    entry.updatedAt = entry.createdAt;
                    entry.views = 0;
                    entry.author = user.email;
                    
                    var Entry = firebase.database().ref('Entry');
                    
                    Entry.push(entry).then(function(data){
                        window.location.href = '/#/entry/'+data.getKey()
                    }).catch(function(error){
                        alert(error);
                        console.error(error);
                    })
                    
                    return false;
                });
                
                
            }else{
                // if not logged in
                alert('Please login first')
                window.location.href = '/#/login';
                
            }
        });
        
    }

}

export default Create;