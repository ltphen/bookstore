let getCards = async () => {
     const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    try {
        const response = await fetch(`http://localhost:3000/spas`, options)
        const json = await response.json();
        // console.log(json)
        return json
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let Profile = {
    render : async () => {

        let cards = await getCards();

        let view =  /*html*/`
        <h1>Firebase - Profile</h1>

        <button id="logout" class="btn btn-lg btn-warning">Logout</button>
        <br>
        <br>

        <pre id="profile"></pre>
        `
        return view
    }
    , after_render: async () => {
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {

                console.log(user);
                document.getElementById('profile').innerHTML = JSON.stringify(user, null, 2);
                document.getElementById('logout').onclick = function () {
                    if (confirm('Logout?')) {
                        firebase.auth().signOut(); // This will trigger onAuthStateChanged again, immediately.
                    }
                };
            } else {
                // if not logged in yet
                window.location.href = 'login.html';

            }
        });
    }

}

export default Profile;