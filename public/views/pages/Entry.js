import Utils from '../../services/Utils.js';


let Entry = {
    render : async () => {


        let view =  /*html*/`
        <article>
        <h1 data-bind="title">Loading...</h1>

        <small>
            By <span data-bind="author"></span> | 
            Updated at <span data-bind="updatedAt-formatted"></span> | 
            <span data-bind="views">0</span> Views
        </small>

        <hr>

        <div data-bind="content"></div>
        
        <hr>
        <div class="text-right">
            <button id="delete" class="btn btn-lg btn-danger">Delete</button>
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="" id="update" class="btn btn-lg btn-primary">Update</a>
        </div>

    </article>
            </section>
        `
        return view
    }
    , after_render: async (id) => {

        var entry_id = id;
        console.log(id)
        if (entry_id) {
            var added_views = false;
            var Entry = firebase.database().ref('Entry').child(entry_id);

            Entry.on('value', function (r) {
                var entry = r.val();

                if (entry) {

                    entry['updatedAt-formatted'] =Utils.datetimeFormat(entry.updatedAt);

                    $('[data-bind]').each(function () {
                        $(this).html(entry[$(this).data('bind')]);
                    });
                    
                    // update title
                    document.title = 'Firebase - ' + entry.title;
                    
                    // increase views count. once.
                    if (!added_views) {
                        added_views = true;
                        Entry.child('views').transaction(function (views) {
                            return (views || 0) + 1;
                        });
                    }
                    
                } else { // content not found
                    //window.location.href = 'index.html';
                }
            });
            
            // update button
            $('#update').attr('href','/#/update/'+entry_id);
            
            // delete button
            $('#delete').click(function(){
                if(confirm('This entry will be permanently delete. Are you sure?')){
                    Entry.remove(); // this will trigger Entry.on('value') immediatly
                }
            });
        } else {
            alert('This entry id does not exist');
            //window.location.href = '/';
            console.log(id)
        }

        

    }

}

export default Entry;