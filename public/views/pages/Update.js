import Utils from '../../services/Utils.js';


let Entry = {
    render : async () => {


        let view =  /*html*/`
        <form id="update_entry">
            <h2>Title</h2>
            <br>
            <input type="text" name="title" class="form-control col-md-12" required>
            
            <br>
            <br>
            
            <h2>Content</h2>
            <br>
            <textarea name="content" id="content"></textarea>
            
            <br>
            <br>
            
            <div class="text-right">
                <button class="btn btn-large btn-primary">Update</button>
            </div>
        </form>

        `
        return view
    }
    , after_render: async (id) => {

        firebase.auth().onAuthStateChanged(function (user) {
            if (user) { 
                
                /*********************************\
                 * Fetch the entry from Firebase *
                \*********************************/
                var entry_id = id;

                if (entry_id) {
                    
                    var Entry = firebase.database().ref('Entry').child(entry_id);

                    Entry.once('value', function (r) { // once = just this once, no need to actively watch the changes
                        var entry = r.val();
                        
                        $('#update_entry [name="title"]').val(entry.title);
                        $('#update_entry [name="content"]').val(entry.content);
                        
                        CKEDITOR.replace('content');
                    });
                    
                    
                    /**********************\
                     * Save the form data *
                    \**********************/
                    $('#update_entry').submit(function(e){
                        e.preventDefault();
                        
                        Entry.transaction(function(entry){
                            
                            entry = entry || {};
                            entry.title = $('#update_entry [name="title"]').val();
                            entry.content = CKEDITOR.instances['content'].getData();
                            entry.updatedAt = new Date().getTime();
                            entry.author = user.email;
                            
                            return entry;
                            
                        }).then(function(){
                            window.location.href = '/#/entry/'+entry_id;
                        }).catch(function(error){
                            alert(error);
                        });

                        return false;
                    });
                    
                }else{
                    window.location.href = '/#/create';
                }
                
                
            }else{
                // if not logged in
                alert('Please log-in')
                window.location.href = '/#/login';
                
            }
        });
        
        

    }

}

export default Entry;