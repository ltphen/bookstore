import Utils from '../../services/Utils.js';



let Home = {
    render : async () => {

        let view =  /*html*/`
        <h1>Firebase - List</h1>

        <div id="entries" class="row">

        </div>
        `
        return view
    }
    , after_render: async () => {
        var Blog = firebase.database().ref('Entry').orderByChild('updatedAt');

        Blog.on('value', function (r) {

            $('#entries').html('Loading...');
            var html = '';
            r.forEach(function (item) {
                let entry = item.val()
                html = '<div class="col-md-4">' +
                    '<a href="/#/entry/' + item.getKey() + '" style="text-decoration:none!important;">' +
                    '<div class="panel panel-info">' +
                    '<div class="panel-heading">' +
                    '<h3 class="panel-title">' + Utils.excerpt(entry.title, 140) + '</h3>' +
                    '</div>' +
                    '<div class="panel-body">' +
                    '<small>By ' + entry.author + ' | ' + Utils.datetimeFormat(entry.updatedAt) + ' | ' + entry.views + ' views</small>' +
                    '<hr><p>' + Utils.excerpt(entry.content, 250) + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</a>' +
                    '</div>' + html; // prepend the entry because we need to display it in reverse order
            });

            $('#entries').html(html);
        });
    }

}

export default Home;