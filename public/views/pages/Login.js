
let Home = {
    render : async () => {
                
        let view =  /*html*/`
        <h1>Firebase - Login</h1>
        
        <div id="firebaseui-auth-container"></div>
        `
        return view
    }
    , after_render: async () => {
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) { // if already logged in
                window.location.href = '/#/profile';
            }
        });
        
        /*******************\
        * init Login UI *
        \*******************/
        
        // FirebaseUI config.
        var uiConfig = {
            'signInSuccessUrl': false,
            'signInOptions': [
                // comment unused sign-in method
                // firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
                //firebase.auth.GithubAuthProvider.PROVIDER_ID,
                firebase.auth.EmailAuthProvider.PROVIDER_ID
            ],
            // Terms of service url.
            'tosUrl': false,
        };
        
        // Initialize the FirebaseUI Widget using Firebase.
        var ui = new firebaseui.auth.AuthUI(firebase.auth());
        // The start method will wait until the DOM is loaded.
        ui.start('#firebaseui-auth-container', uiConfig);
        
        ////////////////////////////////////////
    }
    
}

export default Home;